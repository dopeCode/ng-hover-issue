import { Component, AfterViewInit } from '@angular/core';
import Swiper from 'swiper';
import {TouchService} from "../services/touch.service";

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: [ './nav.component.scss' ]
})
export class NavComponent implements AfterViewInit {
    mySwiper: Swiper;
    slides = [
        'longText0',
        'longText1',
        'longText2',
        'longText3',
        'longText4',
        'longText5',
        'longText6',
        'longText7',
        'longText8'
    ];

    constructor(private touchService: TouchService) {}

    ngAfterViewInit() {
        this.mySwiper = new Swiper('.nav', {
            paginationClickable: false,
            grabCursor: true,
            loop: true,
            autoplay: 1000,
            slidesPerView: 3,
            spaceBetween: 50
        });

        this.mySwiper.on('touchStart', () => {
            this.touchService.triggerTouchStart();
        });
        this.mySwiper.on('touchEnd', () => {
            this.touchService.triggerTouchStop();
        });

    }
}
